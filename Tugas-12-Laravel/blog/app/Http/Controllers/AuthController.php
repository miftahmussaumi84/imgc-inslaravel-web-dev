<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function index()
    {
        return view('register');
    }

    public function register(Request $request)
    {
        $nama_depan = $request->input('nama_depan');
        $nama_blkg = $request->input('nama_blkg');
        $tgl_lahir = $request->input('tgl_lahir');
        $jenis_kelamin = $request->input('jenis_kelamin');

        return redirect('/welcome')
        ->with('nama_depan', $nama_depan)
        ->with('nama_blkg', $nama_blkg)
        ->with('tgl_lahir', $tgl_lahir)
        ->with('jenis_kelamin', $jenis_kelamin);
    }

    public function welcome(Request $request)
    {
        $nama_depan = $request->session()->get('nama_depan');
        $nama_blkg = $request->session()->get('nama_blkg');
        $tgl_lahir = $request->session()->get('tgl_lahir');
        $jenis_kelamin = $request->session()->get('jenis_kelamin');

        return view('welcome', [
            'nama_depan' => $nama_depan,
            'nama_blkg' => $nama_blkg,
            'tgl_lahir' => $tgl_lahir,
            'jenis_kelamin' => $jenis_kelamin,
        ]);
    }
}
