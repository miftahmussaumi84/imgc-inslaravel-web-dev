<?php

require_once './animal.php';

class Frog extends animal
{
	public $jump = "hop hop";

	public function jump() {
		return $this->jump;
	}
}
