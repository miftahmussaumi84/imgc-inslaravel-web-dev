<?php

require './animal.php';
require './Ape.php';
require './Frog.php';

$sheep = new Animal("shaun");

echo "Name :" . $sheep->get_name() . "<br>";
echo "Legs :" . $sheep->get_legs() . "<br>";
echo "Cold Blooded :" . $sheep->cold_blooded() . "<br><br>";

$kodok = new Frog("buduk");
echo "Name :" . $kodok->name . "<br>";
echo "Legs :" . $kodok->legs . "<br>";
echo "Cold Blooded :" . $kodok->cold_blooded() . "<br>";
echo "Jump :" . $kodok->jump() . '<br><br>';

$sungokong = new Ape("kera sakti");
echo "Name :" . $sungokong->name . "<br>"; 
echo "Legs :" . $sungokong->legs . "<br>"; 
echo "Cold Blooded :" . $sungokong->cold_blooded() . "<br>"; 
echo "Yell :" . $sungokong->yell() . '<br><br>';

?>