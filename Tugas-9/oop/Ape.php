<?php

require_once './animal.php';

class Ape extends animal
{
    public $yell = "Auooo";
    public $legs = 2;

    public function yell()
    {
        return $this->yell;
    }
}
